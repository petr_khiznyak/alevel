<?php
$name = 'Петя';
echo $name;
echo '<br>';

$age = 20;
echo $age;
echo '<br>';

$pi = 3.14;
echo $pi;
echo '<br>';

$arr1 = ['alex', 'vova', 'tolya',];
echo '<pre>';
print_r($arr1);
echo '</pre>';

$arr2 = ['alex', 'vova', 'tolya', ['gosha', 'olya']];
echo '<pre>';
print_r($arr2);
echo '</pre>';

$arr3 = ['alex', 'vova', 'tolya', ['gosha', 'olya', ['gosha', milla]]];
echo '<pre>';
print_r($arr3);
echo '</pre>';

$arr4 = [['alex', 'vova', 'tolya',], ['gosha', 'olya', ], ['gosha', milla]];
echo '<pre>';
print_r($arr4);
echo '</pre>';
