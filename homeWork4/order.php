<?php
session_start();
if ($_POST['submit']) {
  if (!empty($_POST['name']) && !empty($_POST['phoneNumber'])) {
    print_r($_SESSION['order'] = [
      'name' => $_POST['name'],
      'phoneNumber' => $_POST['phoneNumber'],
      'delivery' => $_POST['delivery'],
      'address' => $_POST['address'],
      'id' => $_POST['id'],
    ]);
  } else {
    echo 'Some fields is empty';
  }
}

?>
<style>
    form{
        display: flex;
        flex-direction: column;
    }
    input {
        margin-top: 5px;
    }
</style>

<form action="<?= $_SERVER['SCRIPT_NAME']?>" method="post">
  <label for="name">Name</label>
  <input type="text" name="name" id="name">
  <label for="phoneNumber">Phone number</label>
  <input type="tel" name="phoneNumber" id="phoneNumber">
    <label for="delivery">Delivery</label>
    <select name="delivery" id="delivery" >
        <option value="ukrPost">Укр. почта</option>
        <option value="novaiaPost">Новая почта</option>
    </select>
    <label for="address">Address</label>
    <input type="text" name="address" id="address">
    <label for="id">Product ID</label>
    <input type="tel" name="id" id="id">
  <input type="submit" value="Submit" name="submit">
  <input type="reset" value="Reset">
</form>

